# Pedagogic context

(sec:IGE)=

## IGE presentation

&nbsp;&nbsp;&nbsp; The [**Institute for Geosciences and Environmental research**](https://www.ige-grenoble.fr/) is a public research laboratory
supported by [**CNRS**](https://www.cnrs.fr/)/[**INSU**](https://www.insu.cnrs.fr/), [**IRD**](https://www.ird.fr/),
[**UGA**](https://www.univ-grenoble-alpes.fr/) and [**Grenoble-INP**](https://www.grenoble-inp.fr/).
Born from the fusion of the **LGGE** (Laboratory of Glaciology and Geophysics of Environment) and
the **LTHE** (Laboratory of Transfers in Hydrology and Environment) in 2017, the research unit take part of
the [**OSUG**](https://www.osug.fr/) (Observatory for Sciences of Universe of Grenoble) and is involved in several
national and international observation missions. The main fields of the IGE are **Glaciology**, **Hydrology**, **Oceanography**, 
**atmospheric** and **environmental sciences**. Combining these fields with inter and cross-disciplinary sciences due to **human and social sciences** or
**socio-economic actors**, the laboratory studies climate and anthropization of Earth.

The institute is composed of 8 research teams :

* [**CHIANTI**](https://www.ige-grenoble.fr/Chimie-atmospherique-CHIANTI) : Atmospheric Chemistry
* [**CRYODYN**](https://www.ige-grenoble.fr/-Dynamique-des-calottes-et-glaciers-) : Glaciers and ice sheets dynamics
* [**C2H**](https://www.ige-grenoble.fr/-Climat-Cryosphere-Hydrosphere-C2H-) : Climat-Cryosphere-Hydrosphere
* [**HMCIS**](https://www.ige-grenoble.fr/-HydroMeteorologie-Climat-et-Interactions-avec-les-Societes-HMCIS-) : HydroMeteorology, Climat and Interactions in Socities
* [**HyDRIMZ**](https://www.ige-grenoble.fr/-Eau-sol-sediment-qualite-HyDRIMZ-) : Water, Soils, Sediment, Quality
* [**ICE3**](https://www.ige-grenoble.fr/-Carottes-Climat-Chimie-ICE3-) : Ice Cores, Climate, Chemistry
* [**MEOM**](https://www.ige-grenoble.fr/-Modelisation-des-ecoulements-) : Multiscale oceanic flow modeling
* [**PHyREV**](https://www.ige-grenoble.fr/-Processus-Hydrologiques-et-) : Hydrologcal Processes for Vulnerables Water Resources

There are also 3 support services providing the good working of the laboratory :

* Administrative service
* IT service (network, scientific computing, databases ...)
* Technical service :  (drilling, observations, electronics and instrumentation, laboratory analysis, infrastructure)

The global staff of IGE counts around **240 people**:

* 145 permanent members (researchers, lecturers and professors, engineers, technicians and administrative staff)
* 95 PhD students, post-doctoral fellows and staff on fixed-term contracts.

The laboratory is located on the University Campus in Grenoble, divided into 3 sites (Glaciology, OSUG-B and MCP)

(sec:cryodyn)=

## CRYODYN research team

&nbsp;&nbsp;&nbsp; This internship is supervised by **Thomas Chauve** from [CRYODYN](https://www.ige-grenoble.fr/-Dynamique-des-calottes-et-glaciers-). This research team
has for goal to better understand and describe ice dynamics, leading to make better predictions about glaciers and ice caps transformations.
These works are split in 2 research axis :

* [**Study of Glacial processes**](https://www.ige-grenoble.fr/Axe-1-Processus-glaciaires) : understand and describe the physical and mechanical processes controlling the ice dynamics.
* [**Study of the evolution of glaciers and ice sheets**](https://www.ige-grenoble.fr/Axe-2-Evolution-des-glaciers-et) : characterize and predict the evolution of glaciers and ice sheets.

The dynamic recrystallization of ice is an intra-glacial mechanical process and take place into Axe 1.
