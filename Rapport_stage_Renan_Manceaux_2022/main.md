#

::::{grid}
:gutter: 3

:::{grid-item-card}

```{image} ./img/logoIGE_Color_Light.jpg
:alt: logo_ige
:class: bg-primary mb-1
:width: 50%
:align: center
```

:::

:::{grid-item}

:::

:::{grid-item-card}

```{image} ./img/logo_UGA_imag.png
:alt: logo_uga
:class: bg-primary mb-1
:width: 90%
:align: center
```

:::

::::

<style type="text/css">
    table td{
    border:none;
    }
</style>

<table>
    <tr>
        <td>
            <center>
                <h1>Machine Learning to predict location of ice recrystallization</h1> <br>
                <hr>
                May - July 2022 <br>
                UGA and IGE internship <br>
                M1 Statistics and Data Sciences (SSD) <br> <br>
                Renan MANCEAUX <br>
                Supervisor : Thomas CHAUVE
            </center>
        </td>
    </tr>
</table>

-----------------------------------

<center>
    <h2>Abstract</h2>
</center>

&nbsp;&nbsp;&nbsp; Ice is a polycrystalline material composed of grains with an hexagonal crystallographic structure.
The main deformation mecanism in laboratory condition is dislocation creep.
Dynamic recrystallization occurs during deformation, leading to a modification of the microstructure by nucleating new grains with a new orientation.
The location of these new grains is not constrained and lead with other recrystalization mechanism such as grain boundary migration to strong change in mechanical behavior through texture (crytal prefered orientation) development.
Estimate the location of recrystallization area can be helpfull to construct full field recrystalisation model.
We applied machine learning classification methods to predict the location of the recrystallized grains using experimental data (crystalographic orientation mapping, AITA) and full field numerical modelling (CraFT). Indeed, the goal was to indentify pixels in the initial microstructure which will be recrystallized after deformation.
The amont of recrystalized area is low ($4$ to $5 \%$), therefore we worked with unbalanced data.
By comparing prediction results of several distinct models applied with several strategies and by varying parameters, hyperparameters and selected variables,
the classification problem seems to be harder than we thought because of the nature and structure of data. Actually, the similarity of the two classes to identify
seems to be too high to allow to build a model giving a satisfying classification.
In other term, grain germination seems to be too much unpredicatible beyond the distance to grain boundary or triple junction.

-----------------------------------

<center>
    <h2>Acknowledgments</h2>
</center>

&nbsp;&nbsp;&nbsp; I would like to thank Thomas Chauve, at the origin of the intership topic, for his support and his accompaniment all along this 3 months.
Thank you for your patience in explaining to me as a nonphysicist a specific scientific context.

Also, thank you to Maurine Montagnat for her expertise and her comments, leading us to a better evaluation of the range of our results and the impact to the
objectives.

Thanks also to team Cryodyn and to IGE for giving me the opportunity to live this professional experience immersed into another scientific domain.

I want to also thank Jean-François Cœurjolly and Adeline Leclerc-Sanson, in charge of Master SSD, for their follow-up and their implication into
the internship research of their students, without forgetting to thank Pierre Maé for his course of statistical learning which I used as basis all along the internship.

Thank you to all !

-----------------------------------

<center>
    <h2>Table of contents</h2>
</center>

```{tableofcontents}
```
